#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

// this stucture will manage our sprites
typedef struct
{
    const char path[255];       // source image to build the sprite
    int width;                  // x size in pixels of the sprite
    int height;                 // y size in pixels of the sprite
    unsigned int glID;          // do not care (opengl internal texture ID)
} Sprite;


// return a pointer to the sprite built from the image file given in the string path
// return NULL if failed
Sprite * LoadSprite(const char * path);

// load tiled sprite
Sprite * LoadTiledSprite(const char * path,int sx, int sy);

// destroy and cleanup properly a sprite
void destroySprite(Sprite * s);

// draw the sprite centered at x,y position
void drawSprite(Sprite * pSprite, float x, float y, int mirror);    // overloaded version (mirror sprite)


#endif // SPRITE_H_INCLUDED
