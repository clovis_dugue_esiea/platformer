#include "sdl_stuff.h"

// JA 2012 the lab's basecode is inspired by :
//--------------------------------------------
// This code was created by Jeff Molofee '99
// (ported to SDL by Sam Lantinga '2000)
// If you've found this code useful, please let me know.
// Visit me at www.demonews.com/hosted/nehe
//--------------------------------------------



// NB we hide ogl inclusion to simplify things

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
#if defined(__APPLE__) && defined(__MACH__)
#include <OpenGL/gl.h>	// Header File For The OpenGL32 Library
#else
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#endif
#include "SDL/SDL.h"

static Uint8 * keyTrueState = NULL;


int getKeyState(int key)
{
    // todo check for stupid value of key ?
    return keyTrueState[key];
}


int SDL_Loop()
{
    int done = 0;
    // ALLOC ONLY ONCE
    if(! keyTrueState)
    {
        int numkey;
        SDL_GetKeyState(&numkey);
        keyTrueState = (Uint8*) calloc(numkey,sizeof(Uint8));
    }

    SDL_Event event= {0};
    while ( SDL_PollEvent(&event) )
    {
        if ( event.type == SDL_QUIT )
        {
            done = 1;
        }
        if ( event.type == SDL_KEYDOWN )
        {
            keyTrueState[event.key.keysym.sym] = 1;
        }
        if ( event.type == SDL_KEYUP )
        {
            keyTrueState[event.key.keysym.sym] = 0;
        }
    }
    return done;
}


// prepare to draw to the screen
void StartDrawGraphX()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		// Clear The Screen And The Depth Buffer
    glLoadIdentity();				// Reset The View
}

// tell we are done with drawing the current frame
void StopDrawGraphX()
{
    SDL_GL_SwapBuffers();
}


/* A general OpenGL initialization function.  Sets all of the initial parameters. */
void InitGL(int Width, int Height)	        // We call this right after our OpenGL window is created.
{
    glViewport(0, 0, Width, Height);
    glClearColor(0.3f, 0.3f, 0.35f, 1.0f);	// This Will Clear The Background Color To Black
    glClearDepth(1.0);				        // Enables Clearing Of The Depth Buffer

    // desactivate (completely) the z buffer and the z buffer test
    glDepthFunc(GL_LESS);				// The Type Of Depth Test To Do
    glDisable(GL_DEPTH_TEST);			// Enables Depth Testing
    glDepthMask(GL_FALSE);

    glShadeModel(GL_SMOOTH);			// Enables Smooth Color Shading
    glEnable(GL_TEXTURE_2D);


    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);  // Enable Blending for sprites rendering
    glEnable(GL_BLEND);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0.0f,(float)Width,0.0f,(float)Height,-10.0f,10.0f); // juste a simple orthographic projection scaled with our resolution (thous we give everything in screen coordinates)

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}



int InitGraphX(int bitFlags, int sx, int sy )
{
    /* Initialize SDL for video output */
    if ( SDL_Init(SDL_INIT_VIDEO) < 0 )
    {
        fprintf(stderr, "Unable to initialize SDL: %s\n", SDL_GetError());
        exit(1);
    }

    Uint32 sdlflags = SDL_OPENGL;

    if(bitFlags&START_FULLSCREEN)
        sdlflags |=SDL_FULLSCREEN;

    /* Create an OpenGL screen */
    if ( SDL_SetVideoMode(sx, sy, 0, sdlflags ) == NULL )
    {
        fprintf(stderr, "Unable to create OpenGL screen: %s\n", SDL_GetError());
        SDL_Quit();
        exit(2);
    }

    /* Set the title bar in environments that support it */
    SDL_WM_SetCaption("Mario but it's not really Mario", NULL);

    InitGL(sx, sy); // init GL specifics

    return 1;   // everythings went well :)
}

// cleanup
void ShutdownGraphX()
{

    if(keyTrueState)
        free(keyTrueState);
	
    SDL_Quit();
}
