// Used to assert some calls
#include <assert.h>
// Used to delay keypress time
#include <unistd.h>
// Used for drawing grid
#include <GL/gl.h>
#include <GL/glext.h>
#include "sdl_stuff.h"
// Necessary throughout all game
#include "game.h"
#include "editor.h"


// TODO : Ennemy only X
int loadEditorData(Game * pEditor)
{
    assert(pEditor);

    // load all sprites needed
	pEditor->background = LoadSprite("sprites/bg.png");
    pEditor->playerSprite = LoadTiledSprite("sprites/black_mage1x3c.png",3,1);
	pEditor->enemySprite = LoadTiledSprite("sprites/cat_red1x3c.png",3,1);
    pEditor->platformSprite = LoadSprite("sprites/plateform.png");
	pEditor->doorSprite = LoadSprite("sprites/door.png");
	pEditor->heartSprite = LoadSprite("sprites/coracao.png");
	pEditor->teleportSprite = LoadTiledSprite("sprites/teleport1x3.png",3,1);

	// load level..
	printf("Currently loading %s...\n", pEditor->path_lvl);
	pEditor->level = loadLevel(pEditor->path_lvl);
	if (!pEditor->level)
		return 0;
	pEditor->level->offset = buildVect(0.0f, 0.0f);

	// load other ressources...
	loadLevelData(pEditor);

	printf("Loading over\n");
    return 1;
}



int processEditorInput(Game *pEditor, int *memo, int *platform_placing, int *enemy_placing, int *teleport_placing) {
	// to complete
	int ret = SDL_Loop(), x = -1, y = -1;
	char save = 0;
	Uint8 *keystate = SDL_GetKeyState(NULL);

	// p pressed
	if (keystate[SDLK_p] && *memo != 'p') {
		*memo = 'p';
		pEditor->level->platformTab = (Platform *) realloc(pEditor->level->platformTab, (pEditor->level->nbPlatform+2) * sizeof(Platform));
		SDL_GetMouseState(&x, &y);
		convertMouseCoordinates (pEditor->level->offset, &x, &y, &pEditor->params);

		pEditor->level->platformTab[pEditor->level->nbPlatform].pos.x = x;
		pEditor->level->platformTab[pEditor->level->nbPlatform].pos.y = y;
		pEditor->level->platformTab[pEditor->level->nbPlatform].size.x = 1;
		pEditor->level->platformTab[pEditor->level->nbPlatform].size.y = 1;

		printf("First corner placed\n"
		"Place the second corner using 'O'\n"
		"If you only want one block, press 'O' on the same block\n");

		*platform_placing = 1;
		usleep(SLEEP);
	}

	// o pressed
	if (keystate[SDLK_o] && *memo != 'o' && *platform_placing == 1) {
		*memo = 'o';
		SDL_GetMouseState(&x, &y);
		convertMouseCoordinates (pEditor->level->offset, &x, &y, &pEditor->params);

		pEditor->level->platformTab[pEditor->level->nbPlatform].size.x += (x - pEditor->level->platformTab[pEditor->level->nbPlatform].pos.x);
		pEditor->level->platformTab[pEditor->level->nbPlatform].size.y += (y - pEditor->level->platformTab[pEditor->level->nbPlatform].pos.y);
		if (pEditor->level->platformTab[pEditor->level->nbPlatform].size.x < 0) {
			pEditor->level->platformTab[pEditor->level->nbPlatform].pos.x += pEditor->level->platformTab[pEditor->level->nbPlatform].size.x - 1;
			pEditor->level->platformTab[pEditor->level->nbPlatform].size.x = -pEditor->level->platformTab[pEditor->level->nbPlatform].size.x + 2;
		}
		if (pEditor->level->platformTab[pEditor->level->nbPlatform].size.y < 0) {
			pEditor->level->platformTab[pEditor->level->nbPlatform].pos.y += pEditor->level->platformTab[pEditor->level->nbPlatform].size.y - 1;
			pEditor->level->platformTab[pEditor->level->nbPlatform].size.y = -pEditor->level->platformTab[pEditor->level->nbPlatform].size.y + 2;
		}
		if (pEditor->level->platformTab[pEditor->level->nbPlatform].size.x == 0) {
			pEditor->level->platformTab[pEditor->level->nbPlatform].size.x = 2;
			pEditor->level->platformTab[pEditor->level->nbPlatform].pos.x -= 1;
		}
		if (pEditor->level->platformTab[pEditor->level->nbPlatform].size.y == 0) {
			pEditor->level->platformTab[pEditor->level->nbPlatform].size.y = 2;
			pEditor->level->platformTab[pEditor->level->nbPlatform].pos.y -= 1;
		}

		printf("Second corner placed, building...\n");
		printf("NEW PLATFORM : %d %d %d %d\n\n", (int)pEditor->level->platformTab[pEditor->level->nbPlatform].pos.x, (int)pEditor->level->platformTab[pEditor->level->nbPlatform].pos.y, (int)pEditor->level->platformTab[pEditor->level->nbPlatform].size.x, (int)pEditor->level->platformTab[pEditor->level->nbPlatform].size.y);

		pEditor->level->nbPlatform++;
		*platform_placing = 0;
	}
	else if (keystate[SDLK_o] && *memo != 'o' && *platform_placing != 1 && *platform_placing != 3) {
		printf("Place the first corner using 'P' before using 'O'\n"
		"If you only want one block, press 'P' and 'O' on the same block\n\n");
		*platform_placing = 3;
	}


	// t pressed
	if (keystate[SDLK_t] && *memo != 't') {
		*memo = 't';
		pEditor->level->teleport = (Teleport *) realloc(pEditor->level->teleport, (pEditor->level->nbTeleport+2) * sizeof(Teleport));
		SDL_GetMouseState(&x, &y);
		convertMouseCoordinates (pEditor->level->offset, &x, &y, &pEditor->params);

		pEditor->level->teleport[pEditor->level->nbTeleport].tp1pos.x = x+1;
		pEditor->level->teleport[pEditor->level->nbTeleport].tp1pos.y = y+1;
		pEditor->level->teleport[pEditor->level->nbTeleport].tp2pos.x = x+1;
		pEditor->level->teleport[pEditor->level->nbTeleport].tp2pos.y = y+1;

		printf("First teleport placed\n"
		"Place the second corner using 'Y'\n");

		*teleport_placing = 1;
		usleep(SLEEP);
	}

	// y pressed
	if (keystate[SDLK_y] && *memo != 'y' && *teleport_placing == 1) {
		*memo = 'y';
		SDL_GetMouseState(&x, &y);
		convertMouseCoordinates (pEditor->level->offset, &x, &y, &pEditor->params);

		pEditor->level->teleport[pEditor->level->nbTeleport].tp2pos.x = x+1;
		pEditor->level->teleport[pEditor->level->nbTeleport].tp2pos.y = y+1;

		printf("Second teleport placed, building...\n");
		printf("NEW TELEPORT : %d %d %d %d\n\n", (int)pEditor->level->teleport[pEditor->level->nbTeleport].tp1pos.x, (int)pEditor->level->teleport[pEditor->level->nbTeleport].tp1pos.y, (int)pEditor->level->teleport[pEditor->level->nbTeleport].tp2pos.x, (int)pEditor->level->teleport[pEditor->level->nbTeleport].tp2pos.y);

		pEditor->level->nbTeleport++;
		*teleport_placing = 0;
	}
	else if (keystate[SDLK_y] && *memo != 'y' && *teleport_placing != 1 && *teleport_placing != 3) {
		printf("Place the first corner using 'T' before using 'Y'\n\n");
		*teleport_placing = 3;
	}

	// d pressed
	if (keystate[SDLK_d]) {
		*memo = 'd';
		pEditor->level->door = (Door *) realloc(pEditor->level->door, (pEditor->level->nbDoor+2) * sizeof(Door));
		SDL_GetMouseState(&x, &y);
		convertMouseCoordinates (pEditor->level->offset, &x, &y, &pEditor->params);

		pEditor->level->door[pEditor->level->nbDoor].pos.x = x + 1;
		pEditor->level->door[pEditor->level->nbDoor].pos.y = y + 1;

		printf("NEW DOOR : %d %d\n\n", x, y);

		pEditor->level->nbDoor++;
		usleep(SLEEP);
	}

	// s pressed
	if (keystate[SDLK_s]) {
		*memo = 's';
		SDL_GetMouseState(&x, &y);
		convertMouseCoordinates (pEditor->level->offset, &x, &y, &pEditor->params);

		pEditor->level->playerInitialPosition.x = x*BLOC_SIZE + BLOC_SIZE;
		pEditor->level->playerInitialPosition.y = y*BLOC_SIZE + BLOC_SIZE;

		printf("OVERWRITING PLAYER INITIAL POSITION : %d %d\n\n", x, y);

		usleep(SLEEP);
	}

	// e pressed
	if (keystate[SDLK_e] && *memo != 'e') {
		*memo = 'e';
		pEditor->level->enemy = (Enemy *) realloc(pEditor->level->enemy, (pEditor->level->nbEnemy+2) * sizeof(Enemy));
		SDL_GetMouseState(&x, &y);
		convertMouseCoordinates (pEditor->level->offset, &x, &y, &pEditor->params);

		pEditor->level->enemy[pEditor->level->nbEnemy].enemyInitialPosition.x = x*BLOC_SIZE + BLOC_SIZE;
		pEditor->level->enemy[pEditor->level->nbEnemy].enemyInitialPosition.y = y*BLOC_SIZE + BLOC_SIZE;
		pEditor->level->enemy[pEditor->level->nbEnemy].enemyFinalPosition.x = x*BLOC_SIZE + BLOC_SIZE;
		pEditor->level->enemy[pEditor->level->nbEnemy].enemyFinalPosition.y = y*BLOC_SIZE + BLOC_SIZE;

		printf("Initial position of enemy placed\n"
		"Place the final position of enemy using 'R'\n"
		"If you want a static enemy, press 'R' on the same block\n");

		*enemy_placing = 1;
		usleep(SLEEP);
	}

	// r pressed
	if (keystate[SDLK_r] && *memo != 'r' && *enemy_placing == 1) {
		*memo = 'r';
		SDL_GetMouseState(&x, &y);
		convertMouseCoordinates (pEditor->level->offset, &x, &y, &pEditor->params);

		pEditor->level->enemy[pEditor->level->nbEnemy].enemyFinalPosition.x = x*BLOC_SIZE + BLOC_SIZE;
		pEditor->level->enemy[pEditor->level->nbEnemy].enemyFinalPosition.y = pEditor->level->enemy[pEditor->level->nbEnemy].enemyInitialPosition.y;

		printf("Final enemy placed, building...\n");
		if (pEditor->level->enemy[pEditor->level->nbEnemy].enemyInitialPosition.x == pEditor->level->enemy[pEditor->level->nbEnemy].enemyFinalPosition.x && pEditor->level->enemy[pEditor->level->nbEnemy].enemyInitialPosition.y == pEditor->level->enemy[pEditor->level->nbEnemy].enemyFinalPosition.y) {
			printf("NEW ENEMY AT %d %d DOESN'T MOVE\n\n", x, y);
		}
		else {
			printf("NEW ENEMY MOVES BETWEEN : %d %d AND %d %d\n\n", (int)(pEditor->level->enemy[pEditor->level->nbEnemy].enemyInitialPosition.x-1)/BLOC_SIZE, (int)(pEditor->level->enemy[pEditor->level->nbEnemy].enemyInitialPosition.y-1)/BLOC_SIZE, x, (int)(pEditor->level->enemy[pEditor->level->nbEnemy].enemyFinalPosition.y-1)/BLOC_SIZE);
		}

		pEditor->level->enemy[pEditor->level->nbEnemy].init = 0;
		pEditor->level->nbEnemy++;
		*enemy_placing = 0;
	}
	else if (keystate[SDLK_r] && *memo != 'r' && *enemy_placing != 1 && *enemy_placing != 3) {
		printf("Place the initial position of enemy using 'E' before using 'R'\n"
		"If you want a static enemy, press 'E' and 'R' on the same block\n\n");
		*platform_placing = 3;
	}

	// h pressed
	if (keystate[SDLK_h]) {
		*memo = 'h';
		pEditor->level->heart = (Heart *) realloc(pEditor->level->heart, (pEditor->level->nbHeart+2) * sizeof(Heart));
		SDL_GetMouseState(&x, &y);
		convertMouseCoordinates (pEditor->level->offset, &x, &y, &pEditor->params);

		pEditor->level->heart[pEditor->level->nbHeart].pos.x = x + 1;
		pEditor->level->heart[pEditor->level->nbHeart].pos.y = y + 1;

		printf("NEW HEART IN POSITION : %d %d\n\n", x, y);

		pEditor->level->nbHeart++;
		usleep(SLEEP);
	}

	// ESCAPE pressed
	if (keystate[SDLK_ESCAPE] && *memo != 'E') {
		*memo = 'E';
		printf("Escape key pressed, exiting...\n");
		ret = 1;
	}

	// x pressed
	if (keystate[SDLK_x] && *memo != 'x') {
		*memo = 'x';
		printf("X key pressed, saving current config in a new file...\n"
				"Would you really like to save ? [y/n] :\n");
		do {
			scanf(" %c", &save);
			switch (save) {
				case 'y':
					exportFile(pEditor->level);
					break;
				case 'n':
					return 1;
					break;
				default:
					printf("Please enter 'y' or 'n'\n");
					break;
			}
		} while(save != 'y' && save != 'y');
		ret = 1;
	}

	// g pressed
	if (keystate[SDLK_g]) {
		*memo = 'g';
		int i = 0;
		SDL_GetMouseState(&x, &y);
		convertMouseCoordinates (pEditor->level->offset, &x, &y, &pEditor->params);
		char tofree = scanForElements(pEditor->level, x, y, &i);

		switch (tofree) {
			case 0:
				printf("NOTHING TO ERASE\n\n");
				break;
			case 'p':
				printf("ERASED PLATFORM %d %d %d %d\n\n", (int)pEditor->level->platformTab[i].pos.x, (int)pEditor->level->platformTab[i].pos.y, (int)pEditor->level->platformTab[i].size.x, (int)pEditor->level->platformTab[i].size.y);
				break;
			case 'h':
				printf("ERASED HEART %d %d\n\n", (int)pEditor->level->heart[i].pos.x, (int)pEditor->level->heart[i].pos.y);
				break;
			case 'd':
				printf("ERASED DOOR %d %d\n\n", (int)pEditor->level->door[i].pos.x, (int)pEditor->level->door[i].pos.y);
				break;
			case 'e':
				printf("ERASED ENEMY WITH INITIAL POSITION AT %d %d\n", (int)(pEditor->level->enemy[i].enemyInitialPosition.x-1)/BLOC_SIZE, (int)(pEditor->level->enemy[i].enemyInitialPosition.y-1)/BLOC_SIZE);
				break;
			case 't':
				printf("ERASED TELEPORTS AT %d %d AND %d %d\n", (int)pEditor->level->teleport[i].tp1pos.x, (int)pEditor->level->teleport[i].tp1pos.y, (int)pEditor->level->teleport[i].tp2pos.x, (int)pEditor->level->teleport[i].tp2pos.y);
		}

		if (tofree != 0)
			freeItem(pEditor->level, i, tofree);

		usleep(SLEEP);
	}

	// ARROWS pressed
	if (keystate[SDLK_UP] || keystate[SDLK_DOWN] || keystate[SDLK_RIGHT] || keystate[SDLK_LEFT]) {
		if (keystate[SDLK_UP]) {
			*memo = 'U';
			pEditor->level->offset.y -= BLOC_SIZE;
			usleep(SLEEP/2);
		}

		if (keystate[SDLK_DOWN]) {
			*memo = 'D';
			pEditor->level->offset.y += BLOC_SIZE;
			usleep(SLEEP/2);
		}

		if (keystate[SDLK_RIGHT]) {
			*memo = 'R';
			pEditor->level->offset.x -= BLOC_SIZE;
			usleep(SLEEP/2);
		}

		if (keystate[SDLK_LEFT]) {
			*memo = 'L';
			pEditor->level->offset.x += BLOC_SIZE;
			usleep(SLEEP/2);
		}
	}

	if (keystate[SDLK_SPACE]) {
		*memo = 'S';

		pEditor->level->offset.x = 0;
		pEditor->level->offset.y = 0;
	}

	return ret;
}



void convertMouseCoordinates (Vector offset, int *x, int *y, GlobalParams *params) {
	*x /= BLOC_SIZE;
	*y = abs((*y)-params->screenHeight) / BLOC_SIZE;

	*x -= offset.x / BLOC_SIZE;
	*y -= offset.y / BLOC_SIZE;
}



char scanForElements (Level *lvl, int x, int y, int *index) {
	int i = 0;

	for (i=0; i<lvl->nbPlatform; i++) {
		if (lvl->platformTab[i].pos.x == x && lvl->platformTab[i].pos.y == y) {
			*index = i;
			return 'p';
		}
	}
	for (i=0; i<lvl->nbHeart; i++) {
		if (lvl->heart[i].pos.x-1 == x && lvl->heart[i].pos.y == y+1) {
			*index = i;
			return 'h';
		}
	}
	for (i=0; i<lvl->nbDoor; i++) {
		if (lvl->door[i].pos.x-1 == x && lvl->door[i].pos.y == y+1) {
			*index = i;
			return 'd';
		}
	}
	for (i=0; i<lvl->nbEnemy; i++) {
		if (((int)(lvl->enemy[i].enemyInitialPosition.x-1)/BLOC_SIZE == x && (int)(lvl->enemy[i].enemyInitialPosition.y-1)/BLOC_SIZE == y) || ((int)(lvl->enemy[i].enemyFinalPosition.x-1)/BLOC_SIZE == x && (int)(lvl->enemy[i].enemyFinalPosition.y-1)/BLOC_SIZE == y)) {
			*index = i;
			return 'e';
		}
	}
	for (i=0; i<lvl->nbTeleport; i++) {
		if ((lvl->teleport[i].tp1pos.x-1 == x && lvl->teleport[i].tp1pos.y-1) || (lvl->teleport[i].tp2pos.x-1 == x && lvl->teleport[i].tp2pos.y-1)) {
			*index = i;
			return 't';
		}
	}

	return 0;
}



void drawGrid (GlobalParams *params) {
	int i = 0, j = 0;
	Sprite *p = LoadSprite("sprites/dot.png");

	for (i=BLOC_SIZE; i<params->screenWidth; i+=BLOC_SIZE) {
		for (j=BLOC_SIZE; j<params->screenHeight; j+=BLOC_SIZE) {

			glBindTexture(GL_TEXTURE_2D, p->glID);
			glBegin(GL_QUADS);
			{
				glTexCoord2f(0.0f, 0.0f);
				glVertex3f(i-p->width/2.0f, j-p->height/2.0f, 0.0f);
				glTexCoord2f(1.0f, 0.0f);
				glVertex3f(i+p->width/2, j-p->height/2.0f, 0.0f);
				glTexCoord2f(1.0f, 1.0f);
				glVertex3f(i+p->width/2, j+p->height/2.0f, 0.0f);
				glTexCoord2f(0.0f, 1.0f);
				glVertex3f(i-p->width/2, j+p->height/2.0f, 0.0f);
			}
			glEnd();//*/
		}
	}

	destroySprite(p);
}



void exportFile (Level *lvl) {
	char name[64] = "levels/", overwrite = 0, leave = 0;
	int i = 0, miny = 1000000, minx = 1000000;

	// Prompting file name, checking for an overwrite
	printf("File name ? Can only be 56 characters long\n");
	scanf(" %56s", &name[7]);
	FILE *output = fopen(name, "r");
	if (output) {
		printf("File with the same name found. Overwrite ? [y/n] :\n");
		do {
			scanf(" %c", &overwrite);
			switch (overwrite) {
				case 'n':
					printf("Use an other name ? WARNING : WILL OVERWRITE THIS FILE WITHOUT ASKING [y/n] :\n");
					do {
						scanf(" %c", &leave);
						switch (leave) {
							case 'n' :
								printf("Leaving without saving...\n"); // TODO : demander un nouveau nom et ouvrir anyway
								return;
								break;
							case 'y' :
								printf("File name ? Can only be 56 characters long\n");
								scanf(" %56s", &name[7]);
								break;
							default :
								break;
						}
					} while (leave != 'y' && leave != 'n');
					overwrite = 'y';
				case 'y':
					break;
				default:
					printf("Please enter 'y' or 'n'\n");
					break;
			}
		} while (overwrite != 'y' && overwrite != 'y');
	}
	output = fopen(name, "w");
	overwrite = 'y';

	// Finding minimum coordinates for any object
	for (i=0; i<lvl->nbPlatform; i++)
	{
		if (lvl->platformTab[i].pos.x < minx) {
			minx = lvl->platformTab[i].pos.x;
		}
		if (lvl->platformTab[i].pos.y < miny) {
			miny = lvl->platformTab[i].pos.y;
		}
	}

	// Converting all the coordinates
	for (i=0; i<lvl->nbPlatform; i++) {
		lvl->platformTab[i].pos.x += -minx;
		lvl->platformTab[i].pos.y += -miny;
	}
	for (i=0; i<lvl->nbHeart; i++) {
		lvl->heart[i].pos.x += -minx;
		lvl->heart[i].pos.y += -miny;
	}
	for (i=0; i<lvl->nbDoor; i++) {
		lvl->door[i].pos.x += -minx;
		lvl->door[i].pos.y += -miny;
	}
	for (i=0; i<lvl->nbEnemy; i++) {
		lvl->enemy[i].enemyInitialPosition.x += -minx*BLOC_SIZE;
		lvl->enemy[i].enemyFinalPosition.x += -minx*BLOC_SIZE;
		lvl->enemy[i].enemyInitialPosition.y += -miny*BLOC_SIZE;
		lvl->enemy[i].enemyFinalPosition.y += -miny*BLOC_SIZE;
	}
	for (i=0; i<lvl->nbTeleport; i++) {
		lvl->teleport[i].tp1pos.x += -minx;
		lvl->teleport[i].tp2pos.x += -minx;
		lvl->teleport[i].tp1pos.y += -miny;
		lvl->teleport[i].tp2pos.y += -miny;
	}

	// Writing data in the output file
	fprintf(output, "INITPOS %d %d\n\n", (int)lvl->playerInitialPosition.x + BLOC_SIZE, (int)lvl->playerInitialPosition.y + BLOC_SIZE);
	for (i=0; i<lvl->nbPlatform; i++)
		fprintf(output, "PLATFORM %d %d %d %d\n", (int)lvl->platformTab[i].pos.x, (int)lvl->platformTab[i].pos.y, (int)lvl->platformTab[i].size.x, (int)lvl->platformTab[i].size.y);
	printf("\n");
	for (i=0; i<lvl->nbHeart; i++)
		fprintf(output, "HEART %d %d\n", (int)lvl->heart[i].pos.x, (int)lvl->heart[i].pos.y);
	printf("\n");
	for (i=0; i<lvl->nbDoor; i++)
		fprintf(output, "DOOR %d %d\n", (int)lvl->door[i].pos.x, (int)lvl->door[i].pos.y);
	printf("\n");
	for (i=0; i<lvl->nbEnemy; i++)
		fprintf(output, "ENEMY %d %d %d %d\n", (int)lvl->enemy[i].enemyInitialPosition.x/BLOC_SIZE, (int)lvl->enemy[i].enemyInitialPosition.y/BLOC_SIZE, (int)lvl->enemy[i].enemyFinalPosition.x/BLOC_SIZE, (int)lvl->enemy[i].enemyFinalPosition.y/BLOC_SIZE);
	printf("\n");
	for (i=0; i<lvl->nbTeleport; i++)
		fprintf(output, "TELEPORT %d %d %d %d\n", (int)lvl->teleport[i].tp1pos.x, (int)lvl->teleport[i].tp1pos.y, (int)lvl->teleport[i].tp2pos.x, (int)lvl->teleport[i].tp2pos.y);

	// Cleanup
	if (output)
		fclose(output);
}
