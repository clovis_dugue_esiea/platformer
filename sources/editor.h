#ifndef EDITOR_H_INCLUDED
#define EDITOR_H_INCLUDED

// Necessary throughout all game
#include "game.h"


#define SLEEP 300000

// Prototypes
int loadEditorData(Game * pEditor);
int processEditorInput(Game * pEditor, int *memo, int *platform_placing, int *ennemy_placing, int *teleport_placing);
void convertMouseCoordinates (Vector offset, int *x, int *y, GlobalParams *params);
void drawGrid (GlobalParams *params);
void exportFile (Level *lvl);
char scanForElements (Level *lvl, int x, int y, int *index);

#endif
