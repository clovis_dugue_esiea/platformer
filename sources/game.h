#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

// Necessary throughout all game
#include "sprite.h"
#include "vector2d.h"

#define BLOC_SIZE 24



// Default structures
typedef struct
{
    int screenWidth;
    int screenHeight;

    float playerSpeed;
    float playerJumpSpeed;
    float gravity;
	float friction;

	int level_lag;
} GlobalParams;



typedef struct
{
    Vector pos;          // position of the entity on the screen
    Sprite * pSprite;   // we use a pointer to the associated sprite, this is to avoid to reload the same sprite multiple time
} Entity;



typedef enum
{
    DLEFT,
    DRIGHT
} Direction;



typedef struct
{
    Vector pos;     // position of the player on the screen
    Vector vel;     // velocity
    Vector size;
    int animID;
	int init;
    Direction dir;  // direction for annimation (set by last arrow key hit)
    Entity entity;

	int life;
	double inv_frames;
} Player;



typedef struct
{
    Vector pos;      // position in block unit
    Vector size;    // size in block unit ! (to match with an integer number of sprite)
} Platform;



typedef struct
{
	Vector pos;
	Vector size;
	int animID;
	Entity entity;

	int init;
} Life;



// Custom structures
typedef struct
{
	Vector enemyInitialPosition;
	Vector enemyFinalPosition;
    Vector pos;
    Vector vel;
    Vector size;
    int animID;
    int init;
    Direction dir;
    Entity entity;
} Enemy;



typedef struct
{
	Vector pos;
} Door;



typedef struct
{
	Vector pos;
} Heart;



typedef struct
{
	Vector tp1pos;
	Vector tp2pos;

	Vector lag;

	int animID;
	Entity entity;
} Teleport;



// Game structures
typedef struct
{
    Vector playerInitialPosition;  // initial position of thenem player in the level
	int printed;

    Platform * platformTab; // tab of platforms: up to 255
	Door * door;
	Heart * heart;
	Enemy * enemy;
	Teleport *teleport;
    int nbPlatform; // nb of platform used
	int nbDoor;
	int nbHeart;
	int nbEnemy;
	int nbTeleport;
    Sprite * platformSprite;
	Sprite * doorSprite;
	Sprite * heartSprite;
	Sprite * enemySprite;
	Sprite * teleportSprite;;

	Vector offset; // Used for scrolling
} Level;



typedef struct
{
    GlobalParams params;

	Sprite * background;
	Sprite * levelSprite;
	Sprite * enemySprite;
    Sprite * platformSprite;
	Sprite * doorSprite;
	Sprite * heartSprite;
	Sprite * playerSprite;
	Sprite * lifeSprite;
	Sprite * teleportSprite;

    Player player;
	Life life;

	char *path_lvl;
	char *path_spritelvl;

	int nbLevel;
    Level * level;
	char mode;
} Game;



//-----------------------------



// init and load game data
int loadGameData(Game * pGame);
void destroyGameData(Game * pGame);

// load a level from a file
Level * loadLevel(const char * path);
int loadLevelData(Game * pGame);
int destroyLevel(Level * lvl);

// delete an item
void freeItem(Level * lvl, int i, char s);

//  Display
void drawGame(Game * pGame);
void drawPlayer(Player * pPlayer);
void drawLevel(Level * lvl);

//  Game logic
int updateGame(Game * pGame);
void getOffset (Game *pGame);
int takeDamage (Game *pGame);
int GameOver(Game * pGame);
int takeTeleport (Teleport *teleport);

// user inputs
int processUserInput(Game * pGame);


#endif // GAME_H_INCLUDED
