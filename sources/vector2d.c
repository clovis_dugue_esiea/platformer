#include "vector2d.h"

Vector buildVect(float x, float y)
{
    Vector v;
    v.x = x;
    v.y = y;
    return v;
}

Vector multScalVect(float s, Vector v)
{
    Vector res;
    res.x = s*(v.x);
    res.y = s*(v.y);
    return res;
}

Vector addVect(Vector v1, Vector v2)
{
    Vector res;
    res.x = v1.x + v2.x;
    res.y = v1.y + v2.y;
    return res ;
}

