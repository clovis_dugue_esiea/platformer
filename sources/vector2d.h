#ifndef VECTOR2D_H_INCLUDED
#define VECTOR2D_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>

struct Vector_decl
{
    float x;
    float y;
};

typedef struct Vector_decl Vector;

Vector buildVect(float x, float y);

Vector multScalVect (float s, Vector v);

Vector addVect( Vector v1, Vector v2);

#endif
