// Used for drawing
#include "sdl_stuff.h"
// Necessary throughout all game
#include "game.h"
#include "sprite.h"


void getOffset (Game *pGame) {
	pGame->level->offset = buildVect(0.0f, 0.0f);
	pGame->level->offset.x = pGame->player.pos.x - pGame->params.screenWidth / 2;
	pGame->level->offset.y = pGame->player.pos.y - pGame->params.screenHeight / 2;

	int i = 0, imaxx = 0, imaxy = 0;

	for (i=0; i<pGame->level->nbPlatform; i++)
	{
		if (pGame->level->platformTab[i].pos.x > pGame->level->platformTab[imaxx].pos.x)
			imaxx = i;
		if (pGame->level->platformTab[i].pos.y > pGame->level->platformTab[imaxy].pos.y)
			imaxy = i;
	}

	if (pGame->level->offset.x < 0)
		pGame->level->offset.x = 0;
	if (pGame->level->offset.y < 0)
		pGame->level->offset.y = 0;

	if (pGame->mode != 'e') {
		if (pGame->level->offset.x + pGame->params.screenWidth > BLOC_SIZE * (pGame->level->platformTab[imaxx].pos.x + pGame->level->platformTab[imaxx].size.x))
		{
			pGame->level->offset.x = BLOC_SIZE * (pGame->level->platformTab[imaxx].pos.x + pGame->level->platformTab[imaxx].size.x) - pGame->params.screenWidth;
		}

		if (pGame->level->offset.y + pGame->params.screenHeight > BLOC_SIZE * (pGame->level->platformTab[imaxy].pos.y + pGame->level->platformTab[imaxy].size.y))
		{
			pGame->level->offset.y = BLOC_SIZE * (pGame->level->platformTab[imaxy].pos.y + pGame->level->platformTab[imaxy].size.y) - pGame->params.screenHeight;
		}
	}

	pGame->level->offset.x = -pGame->level->offset.x;
	pGame->level->offset.y = -pGame->level->offset.y;
}
