// Necessary throughout all game
#include "game.h"
#include "editor.h"
#include <math.h>
// Used for enemy handling
#include <unistd.h>
// Used for allocating dynamically
#include <stdlib.h>
#include <assert.h>
// Used for key handling
#include "sdl_stuff.h"
// Used for time info
#include "fpstimer.h"

#define PI 3.1415926535897932384626433832795
#define CAP_VEL_Y 600
#define MAX_LIFE 3



int loadGameData(Game * pGame)
{
    assert(pGame);

    // init default params
    pGame->params.playerSpeed  = 200.0f; 		// pixels per second
    pGame->params.playerJumpSpeed  = 700.0f; 	// pixels per second
    pGame->params.gravity = 2000.0f; 			// gravity acc: pixels per second�
	pGame->params.friction = 1000.0f;				// horyzontal friction

	// load all sprites needed
	pGame->background = LoadSprite("sprites/bg.png");
//	pGame->levelSprite = LoadSprite(pGame->path_spritelvl);
	pGame->playerSprite = LoadTiledSprite("sprites/black_mage1x3c.png",3,1);
	pGame->enemySprite = LoadTiledSprite("sprites/cat_red1x3c.png",3,1);
	pGame->lifeSprite = LoadTiledSprite("sprites/life4x3c.png",1,4);
	pGame->platformSprite = LoadSprite("sprites/plateform.png");
	pGame->doorSprite = LoadSprite("sprites/door.png");
	pGame->heartSprite = LoadSprite("sprites/coracao.png");
	pGame->teleportSprite = LoadTiledSprite("sprites/teleport1x3.png",3,1);

    // init player data
    pGame->player.animID = 2;
    pGame->player.dir = DLEFT;
    pGame->player.entity.pSprite = pGame->playerSprite;
    pGame->player.size = buildVect(pGame->playerSprite->width,
                                   pGame->playerSprite->height);
	pGame->player.init = 1;

	// load level..
	printf("Currently loading %s...\n", pGame->path_lvl);
	pGame->level = loadLevel(pGame->path_lvl);
	if (!pGame->level)
		return 0;
	pGame->level->offset = buildVect(0.0f, 0.0f);
	pGame->nbLevel = 1;
	loadLevelData(pGame);

	printf("Loading over\n");
    return 1;
}



int loadLevelData(Game * pGame)
{
	int i = 0;

	if (pGame->mode != 'e') {
		// player life data
		pGame->life.animID = 0;
		pGame->life.entity.pSprite = pGame->lifeSprite;
		pGame->life.size = buildVect(pGame->lifeSprite->width, pGame->lifeSprite->height);
		pGame->life.pos.x = (pGame->params.screenWidth)-50;
		pGame->life.pos.y = (pGame->params.screenHeight)-25;
		pGame->life.init = 1;
		pGame->player.life = MAX_LIFE;

		// init enemy data
		for (i=0; i<pGame->level->nbEnemy; i++) {
			pGame->level->enemy[i].entity.pSprite = pGame->enemySprite;
			pGame->level->enemy[i].size = buildVect(pGame->enemySprite->width, pGame->enemySprite->height);
			pGame->level->enemy[i].pos = pGame->level->enemy[i].enemyInitialPosition;
			pGame->level->enemy[i].vel = buildVect(5.0f, 0.0f);
			pGame->level->enemy[i].animID = 2;
			pGame->level->enemy[i].dir = DLEFT;
			pGame->level->enemy[i].init = 1;
		}

		// finalizing
		pGame->player.pos = pGame->level->playerInitialPosition;
		pGame->player.vel.y += -pGame->params.gravity;
		pGame->params.level_lag = 0;
		pGame->level->printed = 0;
		if (pGame->levelSprite)
			destroySprite(pGame->levelSprite);
		pGame->levelSprite = LoadSprite(pGame->path_spritelvl);
	}

	// load other ressources...
	pGame->level->platformSprite = pGame->platformSprite;
	pGame->level->doorSprite = pGame->doorSprite;
	pGame->level->heartSprite = pGame->heartSprite;
	pGame->level->enemySprite = pGame->enemySprite;
	pGame->level->teleportSprite = pGame->teleportSprite;
    return 1;
}

void destroyGameData(Game * pGame)
{
    assert(pGame);

	if (pGame->playerSprite)
		destroySprite(pGame->playerSprite);
	if (pGame->enemySprite)
		destroySprite(pGame->enemySprite);
	if (pGame->platformSprite)
    	destroySprite(pGame->platformSprite);
	if (pGame->doorSprite)
		destroySprite(pGame->doorSprite);
	if (pGame->heartSprite)
		destroySprite(pGame->heartSprite);
	if (pGame->lifeSprite)
		destroySprite(pGame->lifeSprite);
	if (pGame->background)
		destroySprite(pGame->background);
	if (pGame->teleportSprite)
		destroySprite(pGame->teleportSprite);
	if (pGame->levelSprite)
		destroySprite(pGame->levelSprite);

	if (pGame->path_lvl)
		free(pGame->path_lvl);
	if (pGame->path_spritelvl)
		free(pGame->path_spritelvl);
}



/* Function free item */
void freeItem(Level * lvl, int i, char s)
{
	switch (s) {
		case 'p':
			for(; i<lvl->nbPlatform-1; i++)
				lvl->platformTab[i] = lvl->platformTab[i+1];
			lvl->platformTab = (Platform*)realloc(lvl->platformTab, (lvl->nbPlatform)*sizeof(Platform));

			if(!lvl->platformTab)
			{
				printf("Error realloc\n");
				return;
			}

			lvl->nbEnemy--;
			break;

		case 'e':
			for(; i<lvl->nbEnemy-1; i++)
				lvl->enemy[i] = lvl->enemy[i+1];
			lvl->enemy = (Enemy*)realloc(lvl->enemy, (lvl->nbEnemy)*sizeof(Enemy));

			if(!lvl->enemy)
			{
				printf("Error realloc\n");
				return;
			}

			lvl->nbEnemy--;
			break;

		case 'd':
			for(; i<lvl->nbDoor-1; i++)
				lvl->door[i] = lvl->door[i+1];
			lvl->door = (Door*)realloc(lvl->door, (lvl->nbDoor)*sizeof(Door));

			if(!lvl->door)
			{
				printf("Error realloc\n");
				return;
			}

			lvl->nbDoor--;
			break;

		case 'h':
			for(; i<lvl->nbHeart-1; i++)
				lvl->heart[i] = lvl->heart[i+1];

			lvl->heart = (Heart*)realloc(lvl->heart, (lvl->nbHeart)*sizeof(Heart));

			if(!lvl->heart)
			{
				printf("Error realloc\n");
				return;
			}

			lvl->nbHeart--;
			break;

		case 't':
			for (; i<lvl->nbTeleport-1; i++)
				lvl->teleport[i] = lvl->teleport[i+1];

			lvl->teleport = (Teleport*)realloc(lvl->teleport, (lvl->nbTeleport)*sizeof(Teleport));

			if(!lvl->teleport)
			{
				printf("Error realloc\n");
				return;
			}

			lvl->nbTeleport--;
			break;
	}
}



// helper fonction to grab a line from a file f to the buffer buf
int getLine(FILE * f, char * buff)
{
    int index = 0;
    int nb = 0;
    do
    {
        nb = fread(&buff[index],1,1,f);
        index+=nb;
    }
    while((buff[index-1]!='\n')&&(buff[index-1]!=EOF)&&(nb!=0));
    if(index>0)
        buff[index-1]=0; // end of string char

    return index;
}



Level * loadLevel(const char * path)
{
    FILE * file;
    file = fopen(path,"rb");
    if(!file) {
        printf("Error: failed to open the level file, exiting cleanly...\n");
        return NULL;
    }

	Level * lvl = (Level*)calloc(1, sizeof(Level));

    //lvl->playerInitialPosition = buildVect(2*BLOC_SIZE, 2*BLOC_SIZE);
    lvl->nbPlatform = 0;
	lvl->nbDoor = 0;
	lvl->nbHeart = 0;
	lvl->nbEnemy = 0;
	lvl->nbTeleport = 0;
	int iP = 0, iD = 0, iH = 0, iE = 0, iT = 0;

    // Parse the file
    // logic is Get the fist token of the line, then processe the line
    char oneline[256];	// will store one line of the file
    char firstToken[32]="";

	// Counting tab sizes
	while (getLine(file, oneline)) {
        sscanf(oneline,"%s",firstToken);

        if(firstToken[0]=='#' || firstToken[0] == '\n') {
			continue;
		}
        if(strcmp(firstToken,"INITPOS")==0)	{
            continue;
		}
		if(strcmp(firstToken,"ENEMY")==0) {
			lvl->nbEnemy++;
			continue;
		}
       if(strcmp(firstToken,"PLATFORM")==0) {
            lvl->nbPlatform++;
            continue;
        }
		if(strcmp(firstToken,"DOOR")==0) {
			lvl->nbDoor++;
	    	continue;
        }
		if(strcmp(firstToken,"HEART")==0) {
			lvl->nbHeart++;
		    continue;
        }
		if(strcmp(firstToken,"TELEPORT")==0) {
			lvl->nbTeleport++;
			continue;
		}
	}
	rewind (file);

	lvl->platformTab = (Platform*)calloc(lvl->nbPlatform, sizeof(Platform));
	lvl->door = (Door*)calloc(lvl->nbDoor, sizeof(Door));
	lvl->heart = (Heart*)calloc(lvl->nbHeart, sizeof(Heart));
	lvl->enemy = (Enemy*)calloc(lvl->nbEnemy, sizeof(Enemy));
	lvl->teleport = (Teleport*)calloc(lvl->nbTeleport, sizeof(Teleport));

	// Initializes all the tabs
    while(getLine(file, oneline) )
    {
        sscanf(oneline,"%s",firstToken);

        if(firstToken[0]=='#')
            continue;

        if(strcmp(firstToken,"INITPOS")==0)	{
            sscanf(oneline,"INITPOS %f %f",
                   &lvl->playerInitialPosition.x,
                   &lvl->playerInitialPosition.y);
            continue;
		}

		if(strcmp(firstToken,"ENEMY")==0) {
			sscanf(oneline,"ENEMY %f %f %f %f",
					&lvl->enemy[iE].enemyInitialPosition.x,
					&lvl->enemy[iE].enemyInitialPosition.y,
					&lvl->enemy[iE].enemyFinalPosition.x,
					&lvl->enemy[iE].enemyFinalPosition.y);
			lvl->enemy[iE].dir = DLEFT;
			iE++;
			continue;
		}

       if(strcmp(firstToken,"PLATFORM")==0) {
            sscanf(oneline,"PLATFORM %f %f %f %f",
                   &lvl->platformTab[iP].pos.x,
                   &lvl->platformTab[iP].pos.y,
                   &lvl->platformTab[iP].size.x,
                   &lvl->platformTab[iP].size.y);
			iP++;
            continue;
        }

		if(strcmp(firstToken,"DOOR")==0) {
		    sscanf(oneline,"DOOR %f %f",
		    	&lvl->door[iD].pos.x,
                &lvl->door[iD].pos.y);
			iD++;
	    	continue;
        }

		if(strcmp(firstToken,"HEART")==0)
		{
		    sscanf(oneline,"HEART %f %f",
				&lvl->heart[iH].pos.x,
				&lvl->heart[iH].pos.y);
			iH++;
		    continue;
        }

		if(strcmp(firstToken,"TELEPORT")==0)
		{
			sscanf(oneline,"TELEPORT %f %f %f %f",
				&lvl->teleport[iT].tp1pos.x,
				&lvl->teleport[iT].tp1pos.y,
				&lvl->teleport[iT].tp2pos.x,
				&lvl->teleport[iT].tp2pos.y);
			lvl->teleport[iT].lag = buildVect(0, 0);
			iT++;
			continue;
		}
    }

    return lvl;
}



int destroyLevel(Level * lvl)
{
    if (lvl) {
		if (lvl->platformTab)
			free(lvl->platformTab);
		if (lvl->door)
			free(lvl->door);
		if (lvl->heart)
			free(lvl->heart);
		if (lvl->enemy)
			free(lvl->enemy);
		if (lvl->teleport)
			free(lvl->teleport);
		free(lvl);
	}

    return 1;
}



void drawPlayer(Player * pPlayer)
{
    drawSprite(&pPlayer->entity.pSprite[pPlayer->animID],
               pPlayer->pos.x,
               pPlayer->pos.y,
               (int)pPlayer->dir
              );
}

void drawLife(Life * pLife)
{
    drawSprite(&pLife->entity.pSprite[pLife->animID],
               pLife->pos.x,
               pLife->pos.y,
               0
              );
}

void drawLevel(Level * lvl)
{
}



void drawGame(Game * pGame)
{
	int i = 0, j = 0, k = 0;
	if (pGame->mode == 'p')
		getOffset(pGame);

    StartDrawGraphX();

	drawSprite(pGame->background, pGame->params.screenWidth/2, pGame->params.screenHeight/2, 0);

	if (((pGame->params.level_lag == 0 || fpsGetElapsed() - pGame->params.level_lag > 5) && pGame->mode != 'e') && pGame->level->printed == 0) {
        pGame->params.level_lag = fpsGetElapsed();
        drawSprite(pGame->levelSprite, pGame->params.screenWidth/2, pGame->params.screenHeight/2, 0);
		pGame->level->printed = 1;
    }

	if (pGame->mode == 'e') {
		drawSprite(pGame->playerSprite, pGame->level->playerInitialPosition.x + pGame->level->offset.x, pGame->level->playerInitialPosition.y + pGame->level->offset.y, 0);
		for (i=0; i<pGame->level->nbEnemy; i++) {
			if (pGame->level->enemy[i].enemyInitialPosition.x == pGame->level->enemy[i].enemyFinalPosition.x) {
				drawSprite(pGame->enemySprite, pGame->level->enemy[i].enemyInitialPosition.x + pGame->level->offset.x, pGame->level->enemy[i].enemyInitialPosition.y + pGame->level->offset.y, DLEFT);
			}
			else if (pGame->level->enemy[i].enemyInitialPosition.x > pGame->level->enemy[i].enemyFinalPosition.x) {
				drawSprite(pGame->enemySprite, pGame->level->enemy[i].enemyInitialPosition.x + pGame->level->offset.x, pGame->level->enemy[i].enemyInitialPosition.y + pGame->level->offset.y, DLEFT);
				drawSprite(pGame->enemySprite, pGame->level->enemy[i].enemyFinalPosition.x + pGame->level->offset.x, pGame->level->enemy[i].enemyFinalPosition.y + pGame->level->offset.y, DRIGHT);
			}
			else {
				drawSprite(pGame->enemySprite, pGame->level->enemy[i].enemyInitialPosition.x + pGame->level->offset.x, pGame->level->enemy[i].enemyInitialPosition.y + pGame->level->offset.y, DRIGHT);
				drawSprite(pGame->enemySprite, pGame->level->enemy[i].enemyFinalPosition.x + pGame->level->offset.x, pGame->level->enemy[i].enemyFinalPosition.y + pGame->level->offset.y, DLEFT);
			}
		}
	}

	for(i = 0; i<pGame->level->nbPlatform; i++)
    {
        for (j = 0; j < pGame->level->platformTab[i].size.x; j++)
        {
            for (k = 0; k < pGame->level->platformTab[i].size.y ; k++)
            {
                drawSprite(pGame->platformSprite,
                    (pGame->level->platformTab[i].pos.x+j)*(BLOC_SIZE)+BLOC_SIZE/2 + pGame->level->offset.x,
                    (pGame->level->platformTab[i].pos.y+k)*(BLOC_SIZE)+BLOC_SIZE/2 + pGame->level->offset.y,
                    0);
            }
        }

    }

	for(i=0; i<pGame->level->nbDoor; i++)
		drawSprite(pGame->level->doorSprite, pGame->level->door[i].pos.x*(BLOC_SIZE) + pGame->level->offset.x, pGame->level->door[i].pos.y*(BLOC_SIZE) + pGame->level->offset.y, 0);

	for(i=0; i<pGame->level->nbHeart; i++)
		drawSprite(pGame->level->heartSprite, pGame->level->heart[i].pos.x*(BLOC_SIZE) + pGame->level->offset.x, pGame->level->heart[i].pos.y*(BLOC_SIZE) + pGame->level->offset.y, 0);

	for (i=0; i<pGame->level->nbTeleport; i++) {
		drawSprite(pGame->level->teleportSprite, pGame->level->teleport[i].tp1pos.x*(BLOC_SIZE) + pGame->level->offset.x, pGame->level->teleport[i].tp1pos.y*(BLOC_SIZE) + pGame->level->offset.y,0);
		drawSprite(pGame->level->teleportSprite, pGame->level->teleport[i].tp2pos.x*(BLOC_SIZE) + pGame->level->offset.x, pGame->level->teleport[i].tp2pos.y*(BLOC_SIZE) + pGame->level->offset.y,0);
	}

	if (pGame->player.init) {
		pGame->player.pos = addVect(pGame->player.pos, pGame->level->offset);
		drawPlayer(&pGame->player);
		pGame->player.pos = addVect(pGame->player.pos, multScalVect(-1,pGame->level->offset));
	}

	for (i=0; i<pGame->level->nbEnemy; i++) {
		if (pGame->level->enemy[i].init) {
			drawSprite(&pGame->level->enemy[i].entity.pSprite[pGame->level->enemy[i].animID],
				(pGame->level->enemy[i].pos.x) * BLOC_SIZE + pGame->level->offset.x,
				(pGame->level->enemy[i].pos.y) * BLOC_SIZE + pGame->level->offset.y,
				(int)pGame->level->enemy[i].dir
			);
		}
	}

	if (pGame->mode == 'p')
		drawLife(&pGame->life);
	else
		drawGrid(&pGame->params);

    StopDrawGraphX();
}



// return 1 if we have to exit
int processUserInput(Game * pGame)
{
    // to complete
    int ret = 0;
	ret = SDL_Loop();

    if(getKeyState(KEY_SHIFT))
    {
    }

    // arrow keys

    //---------
    // KEY UP
    static int keyUPMemo = 0;
    int currentKeyUp = getKeyState(KEY_UP);
    // pressed state
    if( currentKeyUp )
    {

		if (pGame->player.vel.y == 0)
			pGame->player.vel.y = pGame->params.playerJumpSpeed;
    }
    // release event
    if( !currentKeyUp&&keyUPMemo )
	{
    }
    // press event
    if( currentKeyUp&&!keyUPMemo )
    {
		if (pGame->player.vel.y == 0)
			pGame->player.vel.y = pGame->params.playerJumpSpeed;
    }
    // update memo state for next frame
    keyUPMemo = currentKeyUp;


    if ( getKeyState(KEY_DOWN) )
    {
    }

    //---------
    // KEY LEFT
    static int keyLeftMemo = 0;
    int currentKeyLeft = getKeyState(KEY_LEFT);
    // pressed state
    if( currentKeyLeft )
    {
        // executed only if the key is pressed
		pGame->player.dir = DLEFT;
		pGame->player.animID = (int)(fpsGetElapsed()*7.0f)%3;
		pGame->player.vel.x += -pGame->params.playerSpeed;
		if (pGame->player.vel.x < -pGame->params.playerSpeed)
			pGame->player.vel.x = -pGame->params.playerSpeed;
    }
    // release event
    if( !currentKeyLeft&&keyLeftMemo )
    {
        // executed only on the state change (released to pressed)
		pGame->player.animID = 1;
    }
    // press event
    if( currentKeyLeft&&!keyLeftMemo )
    {
        // executed only on the state change (pressed to released)
		pGame->player.vel.x += -pGame->params.playerSpeed;
		if (pGame->player.vel.x < -pGame->params.playerSpeed)
			pGame->player.vel.x = -pGame->params.playerSpeed;
    }
    // update memo state for next frame
    keyLeftMemo = currentKeyLeft;

    //---------
    // KEY Right
    static int keyRightMemo = 0;
    int currentKeyRight = getKeyState(KEY_RIGHT);
    // state pressed routine
    if( currentKeyRight )
    {
		pGame->player.dir = DRIGHT;
		pGame->player.animID = (int)(fpsGetElapsed()*7.0f)%3;
		pGame->player.vel.x += pGame->params.playerSpeed;
		if (pGame->player.vel.x > pGame->params.playerSpeed)
			pGame->player.vel.x = pGame->params.playerSpeed;
    }

    if( !currentKeyRight&&keyRightMemo )
    {
		pGame->player.animID = 1;
    }
    if( currentKeyRight&&!keyRightMemo )
    {
		pGame->player.vel.x += pGame->params.playerSpeed;
		if (pGame->player.vel.x > pGame->params.playerSpeed)
			pGame->player.vel.x = pGame->params.playerSpeed;
    }
    // update memo state
    keyRightMemo = currentKeyRight;


    if ( getKeyState(KEY_SPACE) )
    {
        pGame->player.pos = pGame->level->playerInitialPosition;
        pGame->player.vel.y += fpsGetDeltaTime() * -pGame->params.gravity;
    }

    if ( getKeyState(KEY_ENTER) )
    {
    }

    // escape key pressed
    if ( getKeyState(KEY_ESCAPE) )
    {
        printf("Escape key pressed, exiting....\n");
        ret = 1;
    }
    return ret;
}



int updateGame(Game * pGame)
{
	int i = 0;

	// physic update with newton law
	pGame->player.vel.y += fpsGetDeltaTime() * -pGame->params.gravity;
	if (pGame->player.vel.y < -CAP_VEL_Y)
		pGame->player.vel.y = -CAP_VEL_Y;
	if (pGame->player.vel.x > 0)
		pGame->player.vel.x -= fpsGetDeltaTime() * pGame->params.friction;
	if (pGame->player.vel.x < 0)
		pGame->player.vel.x += fpsGetDeltaTime() * pGame->params.friction;
	pGame->player.pos = addVect(pGame->player.pos, multScalVect(fpsGetDeltaTime(), pGame->player.vel));
	if (pGame->player.vel.x > 2*pGame->params.playerSpeed)
		pGame->player.vel.x = 2*pGame->params.playerSpeed;
	if (pGame->player.vel.x < -2*pGame->params.playerSpeed)
		pGame->player.vel.x = -2*pGame->params.playerSpeed;

	// Moving ennemies
	for (i=0; i<pGame->level->nbEnemy; i++) {
		if (pGame->level->enemy[i].enemyInitialPosition.x != pGame->level->enemy[i].enemyFinalPosition.x) {
			if (pGame->level->enemy[i].enemyInitialPosition.x < pGame->level->enemy[i].enemyFinalPosition.x) {
				if (pGame->level->enemy[i].pos.x < pGame->level->enemy[i].enemyInitialPosition.x) {
					pGame->level->enemy[i].pos.x = pGame->level->enemy[i].enemyInitialPosition.x;
					pGame->level->enemy[i].vel.x = -pGame->level->enemy[i].vel.x;
					pGame->level->enemy[i].dir = DRIGHT;
				}
				if (pGame->level->enemy[i].pos.x > pGame->level->enemy[i].enemyFinalPosition.x) {
					pGame->level->enemy[i].pos.x = pGame->level->enemy[i].enemyFinalPosition.x;
					pGame->level->enemy[i].vel.x = -pGame->level->enemy[i].vel.x;
					pGame->level->enemy[i].dir = DLEFT;
				}
			}
			else {
				if (pGame->level->enemy[i].pos.x > pGame->level->enemy[i].enemyInitialPosition.x) {
					pGame->level->enemy[i].pos.x = pGame->level->enemy[i].enemyInitialPosition.x;
					pGame->level->enemy[i].vel.x = -pGame->level->enemy[i].vel.x;
					pGame->level->enemy[i].dir = DLEFT;
				}
				if (pGame->level->enemy[i].pos.x < pGame->level->enemy[i].enemyFinalPosition.x) {
					pGame->level->enemy[i].pos.x = pGame->level->enemy[i].enemyFinalPosition.x;
					pGame->level->enemy[i].vel.x = -pGame->level->enemy[i].vel.x;
					pGame->level->enemy[i].dir = DRIGHT;
				}
			}
			pGame->level->enemy[i].pos = addVect(pGame->level->enemy[i].pos, multScalVect(fpsGetDeltaTime(), pGame->level->enemy[i].vel));
			pGame->level->enemy[i].animID = (int)(fpsGetElapsed()*7.0f)%3;
		}
	}

	// collision with ground and top
	if (pGame->player.pos.y < pGame->playerSprite->height / 2) {
		pGame->player.pos.y = pGame->platformSprite->height;
		pGame->player.vel.y = 0;
	}
	if (pGame->player.pos.y > pGame->params.screenHeight - pGame->playerSprite->height / 2)
		pGame->player.pos.y = pGame->params.screenHeight - pGame->playerSprite->height / 2;

	// collision with each platform in the level
	// forced to check every single platform because of the way the platform position is coded
	float x1, y1, x2, y2, sx1, sy1, sx2, sy2, tx, ty, x3, y3;
	sx2 = pGame->playerSprite->width;
	sy2 = pGame->playerSprite->height;
	x2 = pGame->player.pos.x;
	y2 = pGame->player.pos.y;

	for (i = 0; i < pGame->level->nbPlatform; i++) {
        sx1 = pGame->level->platformTab[i].size.x*BLOC_SIZE;
        sy1 = pGame->level->platformTab[i].size.y*BLOC_SIZE;
		x1 = pGame->level->platformTab[i].pos.x*BLOC_SIZE + sx1/2.f;
        y1 = pGame->level->platformTab[i].pos.y*BLOC_SIZE + sy1/2.f;

		if (fabs(x2-x1)<(sx2+sx1)/2.f && fabs(y2-y1)<(sy2+sy1)/2.f)
        {
			if (x1 <= x2)
				tx = x1 + sx1/2.f - (x2 - sx2/2.f);
			else
				tx = x2 + sx2/2.f - (x1 - sx1/2.f);
			if (y1 <= y2)
				ty = y1 + sy1/2.f - (y2 - sy2/2.f);
			else
				ty = y2 + sy2/2.f - (y1 - sy1/2.f);
            if (tx <= ty)
            {
				// Player R to platform
                if (x2-sx2/2.f < x1+sx1/2.f && x2-sx2/2.f > x1) {
                    pGame->player.pos.x = x1 + sx1/2.f + sx2/2.f;
                    pGame->player.vel.x = 0;
                }
				// Player L to platform
                else if (x2+sx2/2.f > x1-sx1/2.f && x2-sx2/2.f <= x1) {
                    pGame->player.pos.x = x1 - sx1/2.f- sx2/2.f;
                    pGame->player.vel.x = 0;
                }
            }
            else {
				// Player A platform
                if (y2-sy2/2.f < y1+sy1/2.f && y2-sy2/2.f > y1) {
                    pGame->player.pos.y = y1 + (sy1 + sy2)/2.f;
                    pGame->player.vel.y = 0;
                }
				// Player B platform
                else if (y2+sy2/2.f > y1-sy1/2.f && y2-sy2/2.f <= y1) {
                    pGame->player.pos.y = y1 - sy1/2.f - sy2/2.f;
					// So that there is no double jump possible
					pGame->player.vel.y = -0.0001;
                }
            }
        }
	}

	for (i = 0; i < pGame->level->nbHeart; i++) {
	    sx1 = pGame->heartSprite->width;
	    sy1 = pGame->heartSprite->height;
		x1 = pGame->level->heart[i].pos.x*pGame->heartSprite->width + sx1/2.f;
	    y1 = pGame->level->heart[i].pos.y*pGame->heartSprite->height + sy1/2.f;

		if (fabs(x2-x1)<(sx2+sx1)/2.f && fabs(y2-y1)<(sy2+sy1)/2.f)
	    {
			freeItem(pGame->level, i, 'h');

			if(pGame->player.life > 0 && pGame->player.life < MAX_LIFE) {
				pGame->life.animID--;
				pGame->player.life++;
			}
		}
	}


	 for (i = 0; i < pGame->level->nbEnemy; i++) {
		sx1 = pGame->enemySprite->width;
 	    sy1 = pGame->enemySprite->height;
 		x1 = pGame->level->enemy[i].pos.x * pGame->heartSprite->width;
 	    y1 = pGame->level->enemy[i].pos.y * pGame->heartSprite->height;


 		if (fabs(x2-x1)<(sx2+sx1)/2.f && fabs(y2-y1)<(sy2+sy1)/2.f)
 	    {
			if(takeDamage(pGame))
				return 1;

			// Player R to enemy
            if (x2-sx2/2.f < x1+sx1/2.f && x2-sx2/2.f > x1) {
				pGame->player.pos.x = x1+sx1/2.f+sx2/2.f+5;
				pGame->player.vel.x = 2 * pGame->params.playerSpeed;
            }
			// Player L to enemy
            else if (x2+sx2/2.f > x1-sx1/2.f && x2+sx2/2.f <= x1) {
				pGame->player.pos.x = x1-sx1/2.f-sx2/2.f-5;
				pGame->player.vel.x = -2 * pGame->params.playerSpeed;
			}
			// Player A enemy
			if (y2-sy2/2.f <= y1+sy1/2.f && y2-sy2/2.f > y1) {
				pGame->player.pos.y += 1;
				pGame->player.vel.y = 3/4.f * pGame->params.playerJumpSpeed;
			}
 		}
    }

	for (i = 0; i < pGame->level->nbDoor; i++) {
	   sx1 = pGame->doorSprite->width;
	   sy1 = pGame->doorSprite->height;
	   x1 = pGame->level->door[i].pos.x*pGame->heartSprite->width + sx1/2.f;
	   y1 = pGame->level->door[i].pos.y*pGame->heartSprite->height + sy1/2.f;

	   if (fabs(x2-x1)<(sx2+sx1)/2.f && fabs(y2-y1)<(sy2+sy1)/2.f)
	   {
			pGame->nbLevel++;
			sprintf(pGame->path_lvl, "levels/level%d.lvl", pGame->nbLevel);
			sprintf(pGame->path_spritelvl, "sprites/level%d.png", pGame->nbLevel);

			destroyLevel(pGame->level);
			FILE *test_level = fopen(pGame->path_lvl, "r");
			if (!test_level) {
				printf("Congrats, you finished the game ! Go and edit your own levels\n");
				return 1;
			}
			fclose(test_level);
			pGame->level = loadLevel(pGame->path_lvl);
			if (pGame->level == NULL)
				return 1;
			pGame->level->platformSprite = pGame->platformSprite;
			pGame->level->doorSprite = pGame->doorSprite;
			pGame->level->heartSprite = pGame->heartSprite;
			pGame->level->enemySprite = pGame->enemySprite;
			loadLevelData(pGame);

			/* AFFICHAGE DES LEVELS SUIVANTS */
		}
	}

	for (i = 0; i < pGame->level->nbTeleport; i++) {
		sx1 = pGame->teleportSprite->width;
		sy1 = pGame->teleportSprite->height/4.f;
		x1 = pGame->level->teleport[i].tp1pos.x*BLOC_SIZE + sx1/2.f;
		y1 = pGame->level->teleport[i].tp1pos.y*BLOC_SIZE + sy1/2.f;
		x3 = pGame->level->teleport[i].tp2pos.x*BLOC_SIZE + sx1/2.f;
		y3 = pGame->level->teleport[i].tp2pos.y*BLOC_SIZE + sy1/2.f;

		if (fabs(x2-x1)<(sx2+sx1)/2.f && fabs(y2-y1)<(sy2+sy1)/2.f)
		{
				if (pGame->level->teleport[i].lag.x == 0 || fpsGetElapsed() - pGame->level->teleport[i].lag.x > 2) {
					pGame->level->teleport[i].lag.x = fpsGetElapsed();
					pGame->level->teleport[i].lag.y = fpsGetElapsed();
					pGame->player.pos.x = x3;
					pGame->player.pos.y = y3;
				}
		}
		if (fabs(x2-x3)<(sx2+sx1)/2.f && fabs(y2-y3)<(sy2+sy1)/2.f)
		{
			if (pGame->level->teleport[i].lag.y == 0 || fpsGetElapsed() - pGame->level->teleport[i].lag.y > 2) {
				pGame->level->teleport[i].lag.x = fpsGetElapsed();
				pGame->level->teleport[i].lag.y = fpsGetElapsed();
				pGame->player.pos.x = x1;
				pGame->player.pos.y = y1;
			}
		}
	}

	return 0;
}



int takeTeleport (Teleport *teleport) {
	printf("%f\n", teleport->lag.x);
	if (teleport->lag.x == 0) {
        teleport->lag.y = fpsGetElapsed();
		return 0;
    }
    else if (fpsGetElapsed() - teleport->lag.x > 2) {
		teleport->lag.x = 0;
		return 1;
    }

	if (teleport->lag.y == 0) {
        teleport->lag.x = fpsGetElapsed();
		return 0;
    }
	else if (fpsGetElapsed() - teleport->lag.y > 2) {
		teleport->lag.y = 0;
		return 1;
	}

    return 0;
}



int takeDamage (Game *pGame) {
    if (pGame->player.inv_frames == 0) {
        pGame->player.inv_frames = fpsGetElapsed();
        pGame->life.animID++;
        pGame->player.life--;
    }
    else if (fpsGetElapsed() - pGame->player.inv_frames > 0.5) {
        if(pGame->player.life > 0 && pGame->player.life <= MAX_LIFE)
        {
            pGame->player.inv_frames = 0;
        }
        else if (pGame->player.life == 0)
        {
            if(GameOver(pGame)) {
                printf("Game over, exiting cleanly");
                return 1;
            }
            else {
                pGame->nbLevel = 1;
                sprintf(pGame->path_lvl, "levels/level1.lvl");
                destroyLevel(pGame->level);
                pGame->level = loadLevel(pGame->path_lvl);
                loadLevelData(pGame);
            }
        }
    }

    return 0;
}



int GameOver(Game * pGame)
{
	char quit = 0;

    printf("GAME OVER ; Quit or retry ? [q/r] : ");
    do {
        scanf(" %c", &quit);
        switch (quit) {
            case 'q' :
                printf("Leaving...\n");
                return 1;
                break;
            case 'r' :
                printf("Retrying...\n");
                return 0;
                break;
        }
    } while (quit != 'q' && quit != 'r');

	printf("GAME OVER ; Leaving...\n");

    return 1;
}
