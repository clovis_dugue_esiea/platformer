
#ifndef GRAPHX_H_INCLUDED
#define GRAPHX_H_INCLUDED

#include "SDL/SDL.h"


#define START_FULLSCREEN 1
#define START_WINDOWED 0

// initialize the windows/screen with sx and sy defining the width and the height
// if START_FULLSCREEN is passed the video mode is changed to fullscreen
// return 1 for ok, 0 else
int InitGraphX(int bitFlags, int sx, int sy );

// usefull keys for the game
#define KEY_UP SDLK_UP
#define KEY_DOWN SDLK_DOWN
#define KEY_LEFT SDLK_LEFT
#define KEY_RIGHT SDLK_RIGHT
#define KEY_SPACE SDLK_SPACE
#define KEY_ESCAPE SDLK_ESCAPE
#define KEY_ENTER SDLK_RETURN
#define KEY_SHIFT SDLK_LSHIFT

// activate SDL message pump and update events (like key pressed)
int SDL_Loop();

// return 1 if the corresponding key is pressed
int getKeyState(int key);

// prepare to draw the current frame to the screen
void StartDrawGraphX();

// notify we are done with drawing the current frame
// (trigger the display internally)
void StopDrawGraphX();

// do the SDL related cleanup
void ShutdownGraphX();


#endif // GRAPHX_H_INCLUDED
