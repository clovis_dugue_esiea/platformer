#ifndef FPSTIMER_H_INCLUDED
#define FPSTIMER_H_INCLUDED



// get elapsed time since init
double fpsGetElapsed();

// get framerate
float fpsGetFPS();

// get elapsed time in seconds since last call of fpsStep()
double fpsGetDeltaTime();

// update (to be called once a frame)
void fpsStep();

// initialize the internal counters
void fpsInit();




#endif // FPSTIMER_H_INCLUDED
