#include "sprite.h"

// NB we hide ogl inclusion to simplify things

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
#if defined(__APPLE__) && defined(__MACH__)
#include <OpenGL/gl.h>	// Header File For The OpenGL32 Library
#else
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glext.h> // for bgra and clamp
#endif

#include "SDL/SDL.h"
#include "../libraries/FreeImage.h"


#include <assert.h>


// return a pointer to the sprite built from the file given in the string path
// the sprite is loaded in video memory for fast rendering with opengl
// return NULL if failed
Sprite * LoadSprite(const char * path)
{
    FREE_IMAGE_FORMAT format = FreeImage_GetFileType(path, 0);
    FIBITMAP * bitmap = FreeImage_Load(format, path,0);
    if (!bitmap)
    {
        printf("Unable to load Sprite: %s", path);
        return NULL;
    }
    FIBITMAP * bitmap32 = FreeImage_ConvertTo32Bits(bitmap);
    FreeImage_Unload(bitmap);

    Sprite * pSprite = (Sprite * )malloc(sizeof(Sprite));

    pSprite->width = FreeImage_GetWidth(bitmap32);
    pSprite->height = FreeImage_GetHeight(bitmap32);
    glGenTextures(1, &pSprite->glID );
    glBindTexture(GL_TEXTURE_2D, pSprite->glID );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,  pSprite->width , pSprite->height, 0, GL_BGRA, GL_UNSIGNED_BYTE,(GLvoid*)FreeImage_GetBits(bitmap32));
    FreeImage_Unload(bitmap32);   // once the imlage is uploded in video memory as a texture we dont need the bitmap (in ram) anymore
    return pSprite;
}


// return a pointer to the sprite built from the file given in the string path
// return NULL if failed
Sprite * LoadTiledSprite(const char * path,int sx, int sy)
{
    int i, j;
    FREE_IMAGE_FORMAT format = FreeImage_GetFileType(path, 0);
    FIBITMAP *bitmap = FreeImage_Load(format, path,0);
    if (!bitmap)
    {
        printf("Unable to load Sprite: %s", path);
        return NULL;
    }
    FIBITMAP * bitmap32 = FreeImage_ConvertTo32Bits(bitmap);
    FreeImage_Unload(bitmap);
    assert(bitmap32);

    // alloc sx*sy sprites
    Sprite * pSprite = (Sprite * )malloc(sizeof(Sprite)*sx*sy);
    for(j=0; j<sy; j++)
    {
        for(i=0; i<sx; i++)
        {
            // grab the sub bitmap32
            FIBITMAP * subbitmap = FreeImage_Copy(
                                       bitmap32,
                                       i*FreeImage_GetWidth(bitmap32)/sx,
                                       j*FreeImage_GetHeight(bitmap32)/sy,  //top
                                       (i+1)*FreeImage_GetWidth(bitmap32)/sx,
                                       (j+1)*FreeImage_GetHeight(bitmap32)/sy); // bottom

            assert(subbitmap);

            pSprite[j*sx+i].width = FreeImage_GetWidth(subbitmap);
            pSprite[j*sx+i].height = FreeImage_GetHeight(subbitmap);
            glGenTextures(1, &pSprite[j*sx+i].glID );
            glBindTexture(GL_TEXTURE_2D, pSprite[j*sx+i].glID );
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,  pSprite[j*sx+i].width , pSprite[j*sx+i].height, 0, GL_BGRA, GL_UNSIGNED_BYTE,(GLvoid*)FreeImage_GetBits(subbitmap));

            FreeImage_Unload(subbitmap);
        }
    }

    FreeImage_Unload(bitmap32);   // once the imlage is oploded in video memory as a texture we dont need the bitmap anymore
    return pSprite;
}


void destroySprite(Sprite * s)
{
    assert(s);
    // todo destroy opgl texture
    glDeleteTextures( 1, &s->glID );

    free(s);
}


void drawSprite(Sprite * pSprite, float x, float y, int mirror/*=0*/)
{
    assert(pSprite);
    // use the sprite corresponding opengl texture
    glBindTexture(GL_TEXTURE_2D, pSprite->glID);
    // draw a quad mapped with our sprite
    //  glLoadIdentity();
    // handle mirroring the sprite with u texture coordinate
    float u0=0.0f;
    float u1=1.0f;
    if(mirror)
    {
        u0=1.0f;
        u1=0.0f;
    }

    glBegin(GL_QUADS);
    {
        glTexCoord2f(u0, 0.0f);
        glVertex3f(x-pSprite->width/2.0f, y-pSprite->height/2.0f, 0.0f);
        glTexCoord2f(u1, 0.0f);
        glVertex3f(x+pSprite->width/2, y-pSprite->height/2, 0.0f);
        glTexCoord2f(u1, 1.0f);
        glVertex3f(x+pSprite->width/2, y+pSprite->height/2, 0.0f);
        glTexCoord2f(u0, 1.0f);
        glVertex3f(x-pSprite->width/2, y+pSprite->height/2, 0.0f);
    }
    glEnd();

}
