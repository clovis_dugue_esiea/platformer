// Used for time info
#include <time.h>
#include "fpstimer.h"
// Used for drawing
#include "sdl_stuff.h"
// Necessary throughout all game
#include "game.h"
#include "editor.h"
#include "sprite.h"



// just to print time information
void printTimeInfo()
{
    // display time info every 100th frame
    static int count = 0;
    count = (count + 1) % 100;
    if(count== 0)
    {
        // time info
        printf("dt = % lf ", fpsGetDeltaTime());
        printf("t = % lf ", fpsGetElapsed());
        printf("FPS = % f\n", fpsGetFPS());
    }
}



// Used when launching --help option
void printHelp() {
	printf("-h or --help\tDisplays this screen\n\n"
	"-f or --fullscreen\tStarts fullscreen mode (doesn't scale resolution)\n"
	"--level=[FILE.lvl]\tLaunches selected level\n"
	"-e or --editor\tStarts editor\n"
	"--resolution=RESXxRESY\tStarts game with said resolution\n");
}



int main(int argc, char **argv)
{
//	system("clear");
    // static keyword assure that the (big)var game is stored on the heap with a visibility restricted to the main fonction
    static Game game;
	memset(&game, 0, sizeof(Game));
    int videoModeFlag = START_WINDOWED, i = 0, running = 1;
	game.mode = 'p';
	game.params.screenWidth = 648;
	game.params.screenHeight = 480;

    // this is a proper way to parse multiple program's arguments :)
    // you can back up this snipset for use in other projects ^^
    // is there argument(s) to parse ?
    if(argc>=2) {
        for(i = 1; i<argc ; i++)
        {
			// -h or --help : Help screen
			if (strcmp(argv[i], "-h")==0 || strcmp(argv[1], "--help")==0) {
				printHelp();
				return 0;
			}
            // -f or --fullscreen : Launches game in fulscreen mode
            if(strcmp(argv[i],"-f")==0 || strcmp(argv[i],"--fullscreen")==0) {
                // NB: for development and debugging, windowed mode should be prefered
                printf("Starting in fullscreen mode !\n");
                // keep fullscreen flag for init graphX
                videoModeFlag = START_FULLSCREEN;
                continue;  // jump to next for iteration
            }
			// --level=X fetches level from levels folder
			if (strncmp(argv[i], "--level=", 8*sizeof(char))==0) {
				game.path_lvl = (char*) calloc(strlen(argv[i])+5, sizeof(char));
				strcpy(game.path_lvl, "levels/");
				strcpy(&game.path_lvl[7], &argv[i][8]);
				strcpy(&game.path_lvl[strlen(game.path_lvl)], ".lvl");

				game.path_spritelvl = (char*) calloc(strlen("sprites/level1.png")+1, sizeof(char));
				sprintf(game.path_spritelvl, "sprites/level1.png");
				continue;
			}
			// -e or --editor : Launches editor instead of game
			if (strcmp(argv[i], "-e")==0 || strcmp(argv[i], "--editor")==0) {
				printf("Starting editor mode !\n");
				game.mode = 'e';
				continue;
			}
			// --resolution=XxY
			if (strncmp(argv[i], "--resolution=", 13*sizeof(char))==0) {
				sscanf(argv[i], "--resolution=%dx%d", &game.params.screenWidth, &game.params.screenHeight);
				continue;
			}

            // if we reach this section, we face an argument we don't recognize :(
            printf("Unrecognized argument :\"%s\"\n",argv[i]);
			return 0;
        }
    }
	else {
		printHelp();
		printf("\nNo argument found; starting level1 at resolution 648x480\n");
		game.path_lvl = (char*) calloc(strlen("levels/level1.lvl")+1, sizeof(char));
		sprintf(game.path_lvl, "levels/level1.lvl");

		game.path_spritelvl = (char*) calloc(strlen("sprites/level1.png")+1, sizeof(char));
		sprintf(game.path_spritelvl, "sprites/level1.png");
	}

	// Checks for editor + fullscreen
	if (videoModeFlag == START_FULLSCREEN && game.mode == 'e') {
		printf("Editor overrides fullscreen mode to be able to exit in case of crash\n");
		videoModeFlag = START_WINDOWED;
	}
	// Checks for no level input
	if (game.mode == 'p' && !game.path_lvl) {
		game.path_lvl = (char*) calloc(strlen("levels/level1.lvl")+1, sizeof(char));
		sprintf(game.path_lvl, "levels/level1.lvl");

		game.path_spritelvl = (char*) calloc(strlen("sprites/level1.png")+1, sizeof(char));
		sprintf(game.path_spritelvl, "sprites/level1.png");
	}
	// Checks for editor + level
	if (game.mode == 'e' && !game.path_lvl) {
		printf("No level to edit, starting to edit an empty one...\n");
		game.path_lvl = (char*) calloc(strlen("levels/empty.lvl")+1, sizeof(char));
		strcpy(game.path_lvl, "levels/empty.lvl");

		game.path_spritelvl = (char*) calloc(strlen("sprites/level1.png")+1, sizeof(char));
		sprintf(game.path_spritelvl, "sprites/level1.png");
	}
	else if (game.mode == 'e' && game.path_lvl) {
		printf("Editing %s...\n", game.path_lvl);
	}

	// initialize low level video stuff
    InitGraphX(videoModeFlag,game.params.screenWidth,game.params.screenHeight);

	// init time counter
    fpsInit();
	// init random seed (if rand() is needed later)
    srand(time(0));

	switch (game.mode) {
		case 'p':
			// init and load game ressources
			if (!loadGameData(&game))
				return 0;
			while(running) {
				fpsStep();
				//printTimeInfo();
				running = !processUserInput(&game);
				if (updateGame(&game))
					break;
				drawGame(&game);
			}
			break;

		case 'e':
			if (!loadEditorData(&game))
				return 0;
			printf("\nSTART OF EDITOR INSTRUCTIONS : (Might crash; just restart the program)\n"
					"All the positions are determined by the mouse's position relative to the window\n"
					"A border is added around the elements placed, so place a top of level indicator\n\n"
					"'P' places the first corner of a platform\t\t\tSize of sprite : One square block\n"
					"'O' places the opposite corner of a platform\n"
					"'E' places an ennemy\t\t\t\t\t\tSize of sprite : Two square blocks\n"
					"'D' places a door (can play multiple, but not in a row)\t\tSize of sprite : Two square blocks\n"
					"'H' places a heart\t\t\t\t\t\tSize of sprite : Two square blocks\n"
					"'S' places the player spawn\t\t\t\t\tSize of sprite : Two square blocks\n"
					"'G' erases the element(s) the mouse is currently on\n\tPlace the mouse on the lower left corner of the sprite\n"
					"The arrows are used to move the window around in the level\n\n"
					"'ESCAPE' leaves the program without saving and without warning\n"
					"'SPACE' resets the offset\n"
					"'X' leaves the editor and saves"
					"\nEND OF EDITOR INSTRUCTIONS\n\n"
					"START OF EDITOR LOG\n\n");
			int memo = 0, platform_placing = 2, ennemy_placing = 2, teleport_placing = 2;
			game.level->nbPlatform = 0;
			getOffset(&game);
			while (running) {
				fpsStep();
				//printTimeInfo();
				drawGame(&game);
				running = !processEditorInput(&game, &memo, &platform_placing, &ennemy_placing, &teleport_placing);
			}
			printf("\nEND OF EDITOR LOG\n");
			//exit(EXIT_SUCCESS);
			break;
	}

	// Cleanup
    destroyGameData(&game);
    ShutdownGraphX();
    return 0;
}
