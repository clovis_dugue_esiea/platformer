--------------------------------------------------------------------------------

La ligne de commande pour compiler est la suivante :
g++ sources/*.c libraries/libfreeimage.a -o game.exe -Wall -lm -lSDL -lGL

--------------------------------------------------------------------------------

Une fois compilé, le programme propose des options de lancement :

-h or --help
	Affiche un écran explicatif des options de lancement (en anglais)
-f or --fullscreen Starts fullscreen mode
	Lance le jeu en plein écran
--level=[FILE.lvl]
	Lance le niveau correspondant dans le dossier levels/
-e or --editor
	Lance l'éditeur de niveau (les instsructions s'affichent dans le terminal)
	Lancé en plus de l'option --level, il permet d'éditer un niveau existant
	Lancé en plus de l'option --fullscreen, il force le mode fenêtré
--resolution=RESXxRESY
	Lance le jeu avec la résolution donnée par RESX et RESY
	Si le programme est lancé sans cet argument, il se lance en 648x480

--------------------------------------------------------------------------------

Sont nécessaires au fonctionnement (après compilation) seulement les dossiers
"levels" et "sprites". Le dossier "levels" contient 3 exemples de niveaux

Toutes les instructions affichées dans le terminal au cours du programme sont
écrites en anglais, ainsi que les commentaires tout au long du code

Des textures apparaissent dans le coin en bas à gauche une fois en jeu,
simplement les ignorer

D'éventuelles fuites de mémoire et autres erreurs sous Valgrind sont dues à la
librairie SDL et / ou à son implémentation dans le basecode

--------------------------------------------------------------------------------

Instructions de l'éditeur :

Toutes les positions sont déterminées par la position de la souris par rapport
à la fenêtre. Pour toutes les entités, la souris désigne le coin en bas à gauche
de l'entité à placer / effacer
A chaque placement, des informations sont affichées dans le terminal

'P' place le premier coin d'une plateforme
'O' place le coin opposé d'une plateforme
'E' place la position initiale d'un ennemi
'R' place la position finale d'un ennemi
'T' place un premier téléporteur
'Y' place le téléporteur correspondant
'D' place une porte
'H' place un coeur
'S' place le spawn du joueur
'G' permet d'effacer une entité
 	Au cas où il y'a plusieurs entités sur la même case, la priorité est :
	Plateforme, Coeur, Porte, Ennemi, Téléporteur
	La gomme n'arrive pas à effacer des plateformes de taille 1 en X ou en Y
Les flèches permettent de déplacer la fenêtre dans le niveau
'ESPACE' replace la fenêtre à ses coordonnées initiales
'X' quitte l'éditeur de niveau, en proposant de sauvegarder
	On intéragit avec le menu de sauvegarde à travers le terminal
'ESC' quitte le programme sans sauvegarder ni avertir l'utilisateur

--------------------------------------------------------------------------------

Instructions du jeu :

Les flèches permettent au joueur de se déplacer. La caméra le suit
automatiquement.
'ESPACE' place le joueur à sa position initiale dans le niveau. Il faut
appuyer à chaque début de niveau pour pouvoir jouer
'ESC' quitte le programme sans avertir l'utilisateur
