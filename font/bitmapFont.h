
#ifndef BITMAPFONTE_INCLUDED
#define BITMAPFONTE_INCLUDED

#include "sprite.h"
#include <stdarg.h>			// Header File For Variable Argument Routines

typedef struct
{
    Sprite * pSprites;  // sprite tab handling all our character table in small bitmaps
    int pitch;          // pitch of the font (space between chars)

} BFont;

// print a simple string with a previously genrated font at x, y coord on the screen
int BFprint(BFont * pbf, int x, int y, const char * string ); // display string at x,y using pbf Bitmap fonte


// variable arguments version with format string
// warning string limited to 256 characters
// to be used as classical printf
int BFprintf(BFont * pbf, int x, int y, const char * string,... ); // display string at x,y using pbf Bitmap fonte


//-----------------------------------
// creation and destruction functions

// create the fonte from a bitmap containing 16x16 char
BFont * createBitmapFont(const char * fontBitmap, int pitch);
// destroy and cleanup the font
void destroyFont(BFont * pbf);


#endif  //BITMAPFONTE_INCLUDED
