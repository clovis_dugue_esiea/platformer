
#include "bitmapFont.h"
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>


// print a single string
int BFprint(BFont * pbf, int x, int y, const char * string ) // display string at x,y using pbf Bitmap font
{
    int i;
    if(!pbf)
        return 0;

        // wlak through the screen and draw each character
    for(i=0; i<strlen(string); i++)
    {
        drawSprite(&(pbf->pSprites[(int)string[i]]),
                    x+pbf->pitch*i,y,0
                   );
    }
    return 1;
}



// variable arguments version with format string
int BFprintf(BFont * pbf, int x, int y, const char * string, ... ) // display string at x,y using pbf Bitmap font
{

     // todo, check for buffer overflow !!!
    char text[256]="";
	va_list ap;

	if (string == NULL)
		return 0;

	va_start(ap, string);
	    vsprintf(text, string, ap);
	va_end(ap);

     return BFprint(pbf,  x,  y, text );

}


// Create a bitmap font
BFont * createBitmapFont(const char * fontBitmap, int pitch/*=0*/)
{
    BFont * pbf = (BFont *)malloc(sizeof(BFont));
    // should allways be 16x16
    pbf->pitch = pitch;
    pbf->pSprites =  LoadTiledSprite(fontBitmap,16,16);
    if(!pbf->pSprites )
        return NULL;
    return pbf;
}


void destroyFont(BFont * pbf)
{
    destroySprite(pbf->pSprites);
    free(pbf);
}

